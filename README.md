# [frontend.wouterschoofs.be](http://frontend.wouterschoofs.be)

## Installatie

1. Download dit bestand of clone dit via Git naar een map
2. Maak een subdomein aan via uw lokale server (wouterschoofs.dev:8888) en link naar de root van de gekozen map
3. Maak een database aan met naam "school-frontend-2015-2016"
4. Upload de database die ik heb doorgemaild 
5. Ga naar de link van de lokale server

### Opmerking

Ik gebruik een klein beetje back-end omdat ik eigelijk mijn front-end website altijd via [FORK CMS](http://www.fork-cms.com/) maak, dit heeft snel veel voordelen (meertaligheid, cms, SEO optimaliseren,..). Ik heb voor mijn frontend skills een standaard bootstrap gemaakt waarmee ik onderanderen, [Gruntjs](http://gruntjs.com/) (Sass, Jshint, Watch, Livereload,..), [Bootstrap](http://getbootstrap.com/),.. in gebruik zonder dit elke keer opnieuw te moeten instellen.


Ook gebruik ik vaak mijn terminal om snel Grunt tasks te kunnen opzetten, commits te voeren via Git,.. 
Daarvoor gebruik ik [Dotfiles](https://github.com/Sitebase/dotfiles) om aliassen enzover in te stellen.


## Waarom deze website?

Deze website is een strakke maar overzichtelijke website met als onderwerp "ALGEN". Na de lezing over deze Algen werd me snel duidelijk dit de ALGEN voor onze aarde levensbelangrijk zijn. Daarom vergelijk ik het op de website met een Gem(edelsteen). (Ook voor 20% reden dat ik geen mooi icoon vond van Algen :-) )

## Waarmee bewijs ik mijn front-end skills?

1. Favicons / apple touch icons voor verschillende resoluties, apparaten
2. Animerend SVG logo
3. Font icons in footer
4. Navigatie icoon is puur css (werken met :after css selectors)
5. Overwriten van de tekst selector kleur
6. [Smoothstate.js](http://smoothstate.com/) ajax call
7. Responsive
8. Sass/Gruntjs Taskrunner 
9. Mediaqueries
10. [wow.js](http://mynameismatthieu.com/WOW/)
11. Animerende navigatie (responsive)
12. [Capistrano](http://capistranorb.com/) deploy script integratie (ook database deploy)
13. gebruik van: ':before', ':after'
14. Veel gebruik van verschillende positions (absolute, relative,..)
15. HTML5 contactformulier validatie
16. Schuine divs werkend op elke resolutie om de achtergrond kleur te breken (was niet gemakkelijk om dit goed te krijgen :))
17. ..



## Handige mappen constructies voor snel vinden van Scss + Javascript + html

1. src/Frontend/Themes/algen/Core/Layout/Scss -> CSS
2. src/Frontend/Themes/algen/Core/Js -> JS
3. src/Frontend/Themes/algen/Core/Layout/Templates -> HTML (per pagina andere template)




## Extra uitleg

Ik hoop dat het voor jou niet te moeilijk is om er aan uit te kunnen met het Fork CMS. Ik heb het CMS zo weinig mogelijk gebruikt, enkel het contactformulier is een plugin die ik gestyled heb met CSS. Ik heb ook wat html van de plugin veranderd voor het goed te krijgen.
Ik heb zoveel mogelijk verschillende dingen proberen te gebruiken in deze website, maar er zijn wat issues geweest met Smoothstate. Er zijn een heel wat javascript plugins die fouten geven als ik deze Smoothstate.js library gebruik.
Ook met Wordpress geeft het heel wat problemen, bij ForkCMS een paar maar, die ik opgelost heb gekregen. 
- https://wordpress.org/support/topic/doesnt-work-with-smoothstatejs-what-to-call-with-ajax-after-load-of-the-page
- http://stackoverflow.com/questions/28427001/smoothstate-js-stops-other-plugins-from-working-ie-wow-js
- ..
Hier en daar heb ik een wow.js attribute toegevoegd maar zelf vind ik dit niet zo professioneel met dit design, daarom heb ik het niet overal gedaan.
Het is voor 85% zelf geschreven integraties. Geen copy/paste codes gebruikt. 

Waarom ik ook FORKCMS gebruik is omdat dit een Belgisch CMS is (Gent) waar ik achter sta. Ik probeer mee te denken in de toekomst met hun en ga naar de FORKCMS MEETUPS. Dit is opensource, wat ik zeker ook fijn vind om in mijn jeugd aan te werken.


_Wouter Schoofs_ <https://github.com/WouterSchoofs/>
