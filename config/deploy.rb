# With this capistrano plugin, you can pull or push database backups. Very handy!
# Just push your local database to the webserver, or pull (import) the webservers database dump to your local mysql

# https://github.com/parkr/capistrano-slack-notify
require 'capistrano-slack-notify'
# https://github.com/sgruhier/capistrano-db-tasks
require 'capistrano-db-tasks'

# set slack webhook
set :slack_webhook_url,   "https://hooks.slack.com/services/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"
#set slack tasks
before 'deploy', 'slack:starting'
after  'deploy', 'slack:finished'
#set otionally parameters for customize the output
set :slack_room,     '#develop' # defaults to #platform
set :slack_emoji,    ':ghost:' # defaults to :rocket:
set :deployer,       ENV['USER'].capitalize # defaults to ENV['USER']
set :slack_app_name, 'frontend-2015-2016' # defaults to :application
set :slack_color,    false # defaults to true
set :slack_destination, fetch(:stage, 'production') # where your code is going

# set your application name here
set :application, "frontend"
 
# set user to use on server
set :user, "wouterschoofs"
 
# deploy to path (on server)
set :deploy_to, "/home/#{user}/apps/#{application}"             # eg.: /home/sumocoders/apps/sumocoders.be
 
# set document_root
set :document_root, "/home/#{user}/frontend.wouterschoofs.be"            # eg.: /home/sumocoders/default_www
 
# define roles
server "web03.hostbots.be", :app, :web, :db, :primary => true        # eg.: crsolutions.be
 

# git repo & branch
set :repository, "git@bitbucket.org:WouterSchoofs/algen-frontend-2015-2016.git"            # eg.: git@crsolutions.be:sumocoders.be.git
set :branch, "master"
 
# set version control type and copy strategy
set :scm, :git

set :copy_strategy, :checkout
 
# Keep only 3 releases
set :keep_releases, 3
 
# Clean Capistrano releases after deployment. Keep max 2 releases
after "deploy", "deploy:cleanup"
 
# Database stuff
# if you want to remove the dump file after loading
set :rails_env, "production"
set :locals_rails_env, "development"
set :db_local_clean, true  # if you want to remove the database dump file after loading
 
# Load Fork CMS deployment gem (https://github.com/sumocoders/forkcms_deploy)
begin
    require 'forkcms_deploy'
    require 'forkcms_deploy/defaults'                           # optional, contains best practices
rescue LoadError
    $stderr.puts <<-INSTALL
You need the forkcms_deploy gem (which simplifies this Capfile) to deploy this application
Install the gem like this:
    gem install forkcms_deploy
                INSTALL
    exit 1
end
 
# Clear Fork CMS cache after deployment, before symlinking. The Fork gem doesnt clear the cache before deployment so I noticed, so I do it myself.
before "deploy:create_symlink", "forkcms:clear_cache"
namespace :forkcms do
  desc "Clear the Fork CMS cache"
  task :clear_cache do
    run "if test -d #{release_path}; then cd #{release_path}/tools/ && sh remove_cache; fi"
  end
end

# Download/upload Fork CMS files
namespace :files do
  desc "Upload local files to the shared files folder on the server"
  task :push do
    top.upload("src/Frontend/Files/.", "#{shared_path}/files/.", :recursive => true, :mode => '755 -R', :via => :scp)
  end
  
  desc "Download remote files to the local files folder"
  task :pull do
    top.download("#{shared_path}/files/.", "src/Frontend/Files", :via => :scp, :recursive => true)
  end
end