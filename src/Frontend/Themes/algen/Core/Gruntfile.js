module.exports = function(grunt) {
	grunt.initConfig({
		sass: {
		   dist: {
		    files: [{
		      expand: true,
		      cwd: 'Layout/Scss/',
		      src: ['*.scss'],
		      dest: 'Layout/Vendor',
		      ext: '.css'
		    }]
		   }
		},
		cssmin: {
		  target: {
		    files: [{
		      expand: true,
		      cwd: 'Layout/Vendor',
		      src: ['*.css'],
		      dest: 'Layout/Css',
		      ext: '.min.css'
		    }]
		  }
		},
		watch: {
		  options: {
		    livereload: true
		  },
		  css: {
		    files: ['Layout/Scss/*','Layout/Scss/helpers/*'],
		    tasks: ['sass'],
		  },
		  cssmin: {
		    files: ['Layout/Scss/*','Layout/Scss/helpers/*'],
		    tasks: ['cssmin'],
		  },
		},
	});

	grunt.loadNpmTasks('grunt-contrib-sass');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-cssmin');

	grunt.registerTask('default', ['sass','cssmin','watch']);
}