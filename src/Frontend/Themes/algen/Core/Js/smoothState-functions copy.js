jQuery(function(){
  'use strict';
  pageScripts();
  var options = {
  	prefetch: true,
  	cacheLength: 2,
  	onStart: {
  		duration:3000,
  		render: function ($container){
  			$('#content').velocity("fadeOut", { duration: 1500 });
  		}
  	},
  	onReady: {
  		duration: 3000,
  		render: function($container, $newContent){
  			$('#content').velocity("fadeIn", { duration: 1500 });
  			$container.html($newContent);
            pageScripts();
            console.log("test");
  		}
  	}
  },
  smoothState = $('#main').smoothState(options).data('smoothState');
  });
//   var $body = $('html, body'),
//       content = $('#main').smoothState({
//           prefetch: true, 
//           pageCacheSize: 4,
//           debug: true,

//           // in progress of loading
//             onProgress : {
//                 duration: 0, // Duration of the animations, if any.
//                 render: function (url, $container) {
//                     $body.find('a').css('cursor', 'wait');
//                     }

//             },
//             onReady : {
//                 duration:4000, // Duration of the animations, if any.
//                 render: function (url, $container, $content) {
//                     $body.css('cursor', 'auto');
//                     $body.find('a').css('cursor', 'auto');
//                     $container.html($content);
//                     $('#content').velocity("fadeIn", { duration: 3500 });
//                     $(document).ready();
//                     $(window).trigger('load');
//                     pageScripts();
//                     console.log("test");

//                 }
//             },

//             // Runs when a link has been activated
//             onStart: {
//               duration:3000, // Duration of our animation
//               render: function (url, $container) {          
//                 $('#content').velocity("fadeOut", { duration: 1500 });
//               }
//             }
//           }).data('smoothState');

//       //.data('smoothState') makes public methods available
// });

function pageScripts(){
    function fullscreen(){
        jQuery('#hero').css({
            width: jQuery(window).width(),
            height: jQuery(window).height()
        });
	    }
	    fullscreen();
	    // Run the function in case of window resize
	    jQuery(window).resize(function() {
	       fullscreen();         
	    });
	    setInterval(function(){ fullscreen()}, 10);

	var mobileWidth = 820;
	var isMobile = ($( window ).width() < mobileWidth);
	$(document).ready(function(){
	if ($('#navigation').hasClass("active")){       
	    }
	    else{
	    }  
	    $("#navClick").on("click", function(){
	        console.log("navclick");
	    if (isMobile){
	        if( ($("#navigation").hasClass("active")) && ( isMobile) )   {
	            $('#navigation').animate({
	                'margin-left': '-490px',
	                'height': '55px',
	                'overflow': 'visible'

	            }, 600);
	            $("#navigation").removeClass("active");
	        }
	        else{
	            $('#navigation').animate({
	                'margin-left': '0px',
	                'height': '330px'

	            }, 600);
	            $("#navigation").addClass("active")
	        }

	    };
	    if ( !isMobile){
	        if( ($("#navigation").hasClass("active")) && ( !isMobile) )   {
	            $('#navigation').animate({
	                'margin-left': '-685px',
	                'height': '55px',
	                'overflow': 'visible'

	            }, 600);
	            $("#navigation").removeClass("active");
	        }
	        else{
	            $('#navigation').animate({
	                'margin-left': '0px'
	                // 'height': '330px'

	            }, 600);
	            $("#navigation").addClass("active")
	        }

	    };
	    });
});
