jQuery(function(){
  'use strict';
  pageScripts();
  var options = {
  	prefetch: true,
  	cacheLength: 4,
  	onStart: {
  		duration:1050,
  		render: function ($container){
  			$container.addClass('is-exiting');
  			smoothState.restartCSSAnimations();
  		}
  	},
  	onReady: {
  		duration: 1350,
  		render: function($container, $newContent){
  			$container.removeClass('is-exiting');
  			$container.html($newContent);
            pageScripts();
  		}
  	}
  },
  smoothState = $('#main').smoothState(options).data('smoothState');
  });


function pageScripts(){
    function fullscreen(){
        jQuery('#hero').css({
            width: jQuery(window).width(),
            height: jQuery(window).height()
        });
	    }
	    fullscreen();
	    // Run the function in case of window resize
	    jQuery(window).resize(function() {
	       fullscreen();         
	    });
	    setInterval(function(){ fullscreen()}, 10);

	var mobileWidth = 820;
	var isMobile = ($( window ).width() < mobileWidth);


	    $("#navClick").on("click", function(){
		    if (isMobile){
		        if( ($("#navigation").hasClass("active")) && ( isMobile) )   {
		            $('#navigation').animate({
		                'margin-left': '-445px',
		                'height': '55px',
		                'overflow': 'visible'

		            }, 600);
		            $("#navigation").removeClass("active");
		        }
		        else{
		            $('#navigation').animate({
		                'margin-left': '0px',
		                'height': '200px'

		            }, 600);
		            $("#navigation").addClass("active")
		        }

		    };
		    if ( !isMobile){
		        if( ($("#navigation").hasClass("active")) && ( !isMobile) )   {
		            $('#navigation').animate({
		                'margin-left': '-445px',
		                'height': '55px',
		                'overflow': 'visible'

		            }, 600);
		            $("#navigation").removeClass("active");
		        }
		        else{
		            $('#navigation').animate({
		                'margin-left': '0px'
		                // 'height': '330px'

		            }, 600);
		            $("#navigation").addClass("active")
		        }
		    };
	    });
		$("#navigation").on("click", function(){
		        if( ($("#navigation").hasClass("active")) && ( !isMobile) )   {
		            $('#navigation').animate({
		                'margin-left': '-445px',
		                'height': '55px',
		                'overflow': 'visible'

		            }, 600);
		            $("#navigation").removeClass("active");
		        }
		        else{
		            $('#navigation').animate({
		                'margin-left': '0px'
		                // 'height': '330px'

		            }, 600);
		            $("#navigation").addClass("active")
		        }

		});
};
