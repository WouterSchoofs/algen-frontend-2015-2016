{include:Core/Layout/Templates/Head.tpl}

<body class="{$LANGUAGE}" itemscope itemtype="http://schema.org/WebPage">
	{include:Core/Layout/Templates/Cookies.tpl}

		<div class="container-fluid">
			<div class="row vierNulVier">
				<img src="{$THEME_URL}/Core/Layout/images/404.png" alt="404 image"/>
			</div>
		</div><!-- end div id content -->
		<section id="notFoundText">
			<div class="container-fluid">
				{option:positionMain}
					{iteration:positionMain}
		    			{option:positionMain.blockIsHTML}
		    				{$positionMain.blockContent}
		    			{/option:positionMain.blockIsHTML}
					{/iteration:positionMain}
				{/option:positionMain}
			</div>
		</section>
		
	<noscript>
		<div class="message notice">
			<h4>{$lblEnableJavascript|ucfirst}</h4>
			<p>{$msgEnableJavascript}</p>
		</div>
	</noscript>

	<div id="bottomWrapper">
		{include:Core/Layout/Templates/Footer.tpl}
	</div>

	{* General Javascript *}

	{iteration:jsFiles}
		<script src="{$jsFiles.file}"></script>
	{/iteration:jsFiles}

	{* Theme specific Javascript *}
	<script src="{$THEME_URL}/Core/Js/triton.js"></script>

	{* Site wide HTML *}
	{$siteHTMLFooter}
</body>
</html>
