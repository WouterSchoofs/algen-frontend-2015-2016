<footer id="footer">
	<div class="row">
		<div class="container-fluid">
		    <h1 class="wow bounceIn">CONTACT</h1>
		    <div class="line wow fadeIn"></div>
			<div id="socialmediaIcons">
				<a target="_blank" href="https://www.facebook.com/verliefdopjelisa" title="Volg ons op facebook">
					<i class="icon-hup-facebook"></i>
				</a>
				<a target="_blank" href="https://www.instagram.com/wouterschooofs/" title="Volg ons op instagram">
					<i class="icon-hup-insta"></i>
				</a>
				<a target="_blank" href="https://twitter.com/SchoofsWouter" title="Volg ons op twitter">
					<i class="icon-hup-twitter"></i>
				</a>
				<a href="mailto:info@wouterschoofs.be" title="Mail ons">
					<i class="icon-hup-mail"></i>
				</a>
				<a target="_blank" href="https://plus.google.com/+WouterSchoofs/" title="Volg ons op google plus">
					<i class="icon-hup"></i>
				</a>
			</div>
			<div id="text">
				{* Footer position *}
				{iteration:positionFooter}
					{option:positionFooter.blockIsHTML}
						{$positionFooter.blockContent}
					{/option:positionFooter.blockIsHTML}
					{option:!positionFooter.blockIsHTML}
						{$positionFooter.blockContent}
					{/option:!positionFooter.blockIsHTML}
				{/iteration:positionFooter}
			</div>
		</div>
	</div>
</footer>