<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="{$LANGUAGE}" class="ie6"> <![endif]-->
<!--[if IE 7 ]> <html lang="{$LANGUAGE}" class="ie7"> <![endif]-->
<!--[if IE 8 ]> <html lang="{$LANGUAGE}" class="ie8"> <![endif]-->
<!--[if IE 9 ]> <html lang="{$LANGUAGE}" class="ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="{$LANGUAGE}"> <!--<![endif]-->
<head>
	{* Meta *}
	<meta charset="utf-8" />
	<meta name="generator" content="Fork CMS" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta http-equiv="cleartype" content="on">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="HandheldFriendly" content="True">
	{$meta}
	{$metaCustom}

	<title>{$pageTitle}</title>

	{* Favicon and Apple touch icon *}
	<link rel="shortcut icon" href="{$THEME_URL}/favicon.ico" />
    <link rel="apple-touch-icon" sizes="57x57" href="{$THEME_URL}/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="114x114" href="{$THEME_URL}/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="72x72" href="{$THEME_URL}/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="144x144" href="{$THEME_URL}/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="60x60" href="{$THEME_URL}/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="120x120" href="{$THEME_URL}/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="76x76" href="{$THEME_URL}/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="152x152" href="{$THEME_URL}/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="{$THEME_URL}/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="{$THEME_URL}/favicon-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="{$THEME_URL}/favicon-160x160.png" sizes="160x160">
    <link rel="icon" type="image/png" href="{$THEME_URL}/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="{$THEME_URL}/favicon-16x16.png" sizes="16x16">
    <link rel="icon" type="image/png" href="{$THEME_URL}/favicon-32x32.png" sizes="32x32">
<link href='http://fonts.googleapis.com/css?family=Cabin+Condensed:700' rel='stylesheet' type='text/css'>
	{* Windows 8 tile *}
	<meta name="application-name" content="{$siteTitle}"/>
	<meta name="msapplication-TileColor" content="#3380aa"/>
	<meta name="msapplication-TileImage" content="{$THEME_URL}/tile.png"/>
	<!-- OPEN GRAPH DATA FACEBOOK -->
	<meta property="og:title" content="{$siteTitle}" />
	<meta property="og:url" content="http://www.frontend-webdesign-2015-2016.be/" />
	<meta property="og:description" content="frontend-webdesign-2015-2016" /> 
	<meta property="og:site_name" content="frontend-webdesign-2015-2016" />
    <!-- OPEN GRAPH DATA TWITTER -->
    <meta name="twitter:card" content="{$siteTitle}" />
    <meta name="twitter:site" content="-" />
    <meta name="twitter:title" content="-" />
    <meta name="twitter:description" content="-" />
	

	<link rel="stylesheet" href="{$THEME_URL}/Core/Layout/Css/main.min.css" />

	<script src="{$THEME_URL}/Core/Js/jquery.min.js"></script>	
	<script src="{$THEME_URL}/Core/Js/bootstrap.min.js"></script>	

	


	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	{* Site wide HTML *}
	{$siteHTMLHeader}
</head>