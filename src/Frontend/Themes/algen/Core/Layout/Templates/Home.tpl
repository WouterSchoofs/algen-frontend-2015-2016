{include:Core/Layout/Templates/Head.tpl}
<body itemscope itemtype="http://schema.org/WebPage">
	<div id="main" class="m-scene scene_element--fadein">
		<header id="header">
			<div id="logo">
				<a href="/">
					<img src="{$THEME_URL}/Core/Layout/images/logoNav.png" alt="logo wij zijn geen onkruid"/>
				</a>
			</div>
			<div id="navigation">
				<ul>
					<li><a class="clickEvent" href="/">Home</a></li>
					<li><a class="clickEvent" href="/wat-zijn-algen">Wat zijn algen?</a></li>
					<li><a class="clickEvent" href="/waar-leven-algen">Waar leven ze?</a></li>
					<li><a class="clickEvent" href="/contact">Contact</a></li>
				</ul>

			</div>
			<div id="navClick">
				<span id="navIcon"></span>
			</div>
		</header>

		<div class="m-header scene_element scene_element--fadein">
		<section id="hero">
			<div id="logo">
					<object>
						<embed src="{$THEME_URL}/Core/Layout/images/logo-algen-svganimation.svg" alt="logo algen">
					</object>
			</div>
		    <div class="header">
		      <img src="{$THEME_URL}/Core/Layout/images/gem-ripped-algen.png" alt="logo Wij zijn geen onkruid, algenplant"/>
		    </div>
		</section>
		<section id="content">
			<div class="row">
				<div class="container-fluid">
			        <h3 class="wow bounceIn"> WIJ ZIJN </h3>
			        <h1 class="wow bounceIn">ALGEN</h1>
			        <div class="line"></div>
					<div class="text wow bounceInLeft">
						{* TextHomePage position *}
						{iteration:positionTextHomePage}
							{option:positionTextHomePage.blockIsHTML}
								{$positionTextHomePage.blockContent}
							{/option:positionTextHomePage.blockIsHTML}
							{option:!positionTextHomePage.blockIsHTML}
								{$positionTextHomePage.blockContent}
							{/option:!positionTextHomePage.blockIsHTML}
						{/iteration:positionTextHomePage}
					</div>
				</div>
			</div>
		</section>
		
		<div id="bottomWrapper">
			{include:Core/Layout/Templates/Footer.tpl}
		</div>
		</div>
	</div>

	<noscript>
		<div class="message notice">
			<h4>{$lblEnableJavascript|ucfirst}</h4>
			<p>{$msgEnableJavascript}</p>
		</div>
	</noscript>



	{* General Javascript *}
	{iteration:jsFiles}
		<script src="{$jsFiles.file}"></script>
	{/iteration:jsFiles}

	{* Theme specific Javascript *}
	<script src="{$THEME_URL}/Core/Js/jquery.min.js"></script>	
	<script src="{$THEME_URL}/Core/Js/modernizr.custom.js"></script>
	<script src="{$THEME_URL}/Core/Js/jquery.smoothState.min.js"></script>
	<script src="{$THEME_URL}/Core/Js/wow.min.js"></script>
	<script src="{$THEME_URL}/Core/Js/smoothState-functions.js"></script>
	<script src="{$THEME_URL}/Core/Js/triton.js"></script>
	<script>
        new WOW().init();
    </script>


	{* Site wide HTML *}
	{$siteHTMLFooter}
</body>
</html>
