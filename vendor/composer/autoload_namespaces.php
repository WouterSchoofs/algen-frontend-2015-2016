<?php

// autoload_namespaces.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'phpDocumentor' => array($vendorDir . '/phpdocumentor/reflection-docblock/src'),
    'Twig_' => array($vendorDir . '/twig/twig/lib'),
    'Symfony\\Bundle\\MonologBundle' => array($vendorDir . '/symfony/monolog-bundle'),
    'Symfony\\' => array($vendorDir . '/symfony/symfony/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log'),
    'Prophecy\\' => array($vendorDir . '/phpspec/prophecy/src'),
    'Doctrine\\Instantiator\\' => array($vendorDir . '/doctrine/instantiator/src'),
    'Doctrine\\Common\\Lexer\\' => array($vendorDir . '/doctrine/lexer/lib'),
    'Doctrine\\Common\\Inflector\\' => array($vendorDir . '/doctrine/inflector/lib'),
    'Doctrine\\Common\\Collections\\' => array($vendorDir . '/doctrine/collections/lib'),
    'Doctrine\\Common\\Cache\\' => array($vendorDir . '/doctrine/cache/lib'),
    'Doctrine\\Common\\Annotations\\' => array($vendorDir . '/doctrine/annotations/lib'),
    'Doctrine\\Common\\' => array($vendorDir . '/doctrine/common/lib'),
    'Behat\\Transliterator' => array($vendorDir . '/behat/transliterator/src'),
    'Assetic' => array($vendorDir . '/kriswallsmith/assetic/src'),
    '' => array($baseDir . '/src'),
);
